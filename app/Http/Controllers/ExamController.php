<?php

namespace App\Http\Controllers;

use App\Models\exam;
use App\Models\answer;
use App\Models\question;
use App\Models\UserExam;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
    public function index()
    {
        $exams = exam::all()->sortBy('date');
        return view('welcome', ['exams' => $exams]);
    }

    public function show(string $examId)
    {
        $user = Auth::user();
        $exam = exam::where('id', $examId)->firstOrFail();
        $totalQuestion = question::where('exam_id', $examId)->count();
        $userExam = UserExam::where('user_id', $user->id)
            ->where('exam_id', $examId)
            ->orderBy('date', 'desc')
            ->limit(6)
            ->get();

        $labelChart = [];
        foreach ($userExam as $item) {
            array_push($labelChart, date('d M Y h:i', strtotime($item->date)));
        }

        $dataChart = [];
        foreach ($userExam as $item) {
            array_push($dataChart, $item->value);
        }

        return view('exams/index', [
            'exam' => $exam,
            'user_exam' => $userExam,
            'total_question' => $totalQuestion,
            'label_chart' => $labelChart,
            'data_chart' => $dataChart
        ]);
    }

    public function takeQuiz(Request $request, string $examId)
    {
        $exam = exam::findOrFail($examId);
        $questions = question::where('exam_id', $exam->id)->get();

        foreach ($questions as $item) {
            $item->options = [
                $item->option_1,
                $item->option_2,
                $item->option_3,
                $item->option_4
            ];
        }

        return view('exams/quiz', [
            'exam' => $exam,
            'questions' => $questions,
        ]);
    }

    public function processQuiz(Request $request)
    {
        $user = Auth::user();
        $examId = $request->examId;
        $questions = question::where('exam_id', $examId)->get();
        $answers = $request->except('_token', 'examId');
        $totalCorrect = 0;
        $payload = [];

        foreach ($answers as $key => $value) {
            array_push($payload, [
                'id' => Str::uuid(),
                'user_id' => $user->id,
                'question_id' => $key,
                'chosen_option' => $value,
                'value' => '',
            ]);
        }

        foreach ($questions as $question) {
            for ($i = 0; $i < count($payload); $i++) {
                if ($question->id === $payload[$i]['question_id']) {
                    if ($question->correct_option === $payload[$i]['chosen_option']) {
                        $payload[$i]['value'] = 1;
                        $totalCorrect += 1;
                    } else {
                        $payload[$i]['value'] = 0;
                    }
                }
            }
        }

        $totalValue = $totalCorrect * 100 / count($questions);

        answer::insert($payload);
        UserExam::create([
            'user_id' => $user->id,
            'exam_id' => $examId,
            'value' => $totalValue,
            'date' => now()
        ]);

        return redirect()->route('exams.show', [$examId]);
    }
}
