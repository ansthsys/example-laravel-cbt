<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class answer extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'user_id',
        'question_id',
        'chosen_option',
        'value',
    ];
}
