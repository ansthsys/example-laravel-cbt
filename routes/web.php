<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ExamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'getLogin'])->name('login');
    Route::post('/login', [AuthController::class, 'postLogin'])->name('login.post');
    Route::get('/register', [AuthController::class, 'getRegister'])->name('register');
    Route::post('/register', [AuthController::class, 'postRegister'])->name('register.post');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/logout', [AuthController::class, 'postLogout'])->name('logout.post');
    Route::get('/', [ExamController::class, 'index'])->name('dashboard');
    Route::get('/exams/{examId}', [ExamController::class, 'show'])->name('exams.show');
    Route::post('/exams/{examId}', [ExamController::class, 'takeQuiz'])->name('exams.takeQuiz');
    Route::post('/exams/{examId}/process', [ExamController::class, 'processQuiz'])->name('exams.processQuiz');
});
