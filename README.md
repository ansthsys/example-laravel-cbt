# Test Code at INKOR - Laravel CBT (Computer Based Test)

System requirement:
1.  PHP version >= `8.1`
2.  Composer version >= `2.2`
3.  NodeJS version >= `18.17.0`
4.  NPM version >= `9.6.7`
5.  MySQL version >=`5.7`
___

Installation Guide:
1.  Clone repo `https://gitlab.com/ansthsys/example-laravel-cbt`
2.  Change directory into project `cd example-laravel-cbt`
3.  Install dependecies from Composer `composer install`
4.  Install dependecies from NPM `npm install`
5.  Create environment file `cp .env.example .env`
6.  Create application encryption key `php artisan key:generate`
7.  Setup database using environment file
    ```
    DB_DATABASE="Database Name"
    DB_USERNAME="Database Username"
    DB_PASSWORD="Database Password"
    ```
8.  Run migrations and seeders `php artisan migrate --seed`
9.  Compile the Frontend `npm run dev`
10. Run Application `php artisan serve`
