<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class QuestionMatematikaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('questions')->insert([
            [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Suatu hari, Rina membeli 3 buah pensil dengan harga masing-masing Rp 2.500. Berapa total uang yang harus dibayarkan oleh Rina?',
                'option_1' => 'Rp 5.000',
                'option_2' => 'Rp 6.500',
                'option_3' => 'Rp 7.500',
                'option_4' => 'Rp 10.000',
                'correct_option' => 'Rp 7.500',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Ayah memiliki 15 kelereng, sedangkan Budi memiliki 10 kelereng. Berapa jumlah kelereng yang dimiliki mereka secara keseluruhan?',
                'option_1' => '10 kelereng',
                'option_2' => '15 kelereng',
                'option_3' => '20 kelereng',
                'option_4' => '25 kelereng',
                'correct_option' => '25 kelereng',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Ibu membeli 2 kg apel seharga Rp 5.000 per kg, dan 1 kg jeruk seharga Rp 4.000 per kg. Berapa total uang yang harus dibayarkan oleh ibu?',
                'option_1' => 'Rp 9.000',
                'option_2' => 'Rp 10.000',
                'option_3' => 'Rp 11.000',
                'option_4' => 'Rp 14.000',
                'correct_option' => 'Rp 14.000',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Suatu panitia akan membagikan 45 kue kepada anak-anak. Jika setiap anak mendapatkan 3 kue, berapa jumlah anak yang akan mendapatkan kue?',
                'option_1' => '10 anak',
                'option_2' => '15 anak',
                'option_3' => '20 anak',
                'option_4' => '25 anak',
                'correct_option' => '15 anak',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Sebuah persegi panjang memiliki panjang 8 cm dan lebar 5 cm. Luas persegi panjang tersebut adalah...',
                'option_1' => '13 cm²',
                'option_2' => '30 cm²',
                'option_3' => '40 cm²',
                'option_4' => '45 cm²',
                'correct_option' => '40 cm²',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Diketahui 12 + 9 = ...',
                'option_1' => '20',
                'option_2' => '21',
                'option_3' => '22',
                'option_4' => '23',
                'correct_option' => '21',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Urutan bilangan berikutnya dari 2, 4, 6, 8, ... adalah...',
                'option_1' => '9',
                'option_2' => '10',
                'option_3' => '11',
                'option_4' => '12',
                'correct_option' => '10',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Sebuah peternakan memiliki 32 ekor kambing. Kemudian, ada penambahan sebanyak 12 ekor kambing. Berapa jumlah kambing di peternakan tersebut sekarang?',
                'option_1' => '32 ekor',
                'option_2' => '34 ekor',
                'option_3' => '36 ekor',
                'option_4' => '44 ekor',
                'correct_option' => '44 ekor',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Seorang anak memiliki uang sebesar Rp 10.000. Dia membeli permen seharga Rp 2.500 per bungkus. Berapa banyak permen yang dapat dibelinya?',
                'option_1' => '2 permen',
                'option_2' => '3 permen',
                'option_3' => '4 permen',
                'option_4' => '5 permen',
                'correct_option' => '4 permen',
                'created_at' => now(),
                'updated_at' => now(),
            ], [
                'id' => Str::uuid(),
                'exam_id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'question' => 'Sebuah kelas terdiri dari 25 siswa. Jika 3 siswa pindah ke kelas lain, berapa siswa yang tersisa di kelas tersebut?',
                'option_1' => '20 siswa',
                'option_2' => '22 siswa',
                'option_3' => '23 siswa',
                'option_4' => '28 siswa',
                'correct_option' => '22 siswa',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
