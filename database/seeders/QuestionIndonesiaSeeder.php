<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class QuestionIndonesiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('questions')->insert([
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Bendungan di Desa Jatirogo ini tidak ada duanya di Indonesia. Tubuh bendungan tersebut dari bantalan karet berisi air. Karena terbuat dari karet, tinggi permukaannya bisa diatur secara fleksibel. Bila terjadi banjir, bantalan karet itu dikempiskan. Dan air bah lancar mengalir ke laut. Sebaliknya, bila volume air sungai mengecil, tubuh bendungan diisi penuh, sehingga tingginya mencapai 3 m. Sungai terbendung dan airnya dimanfaatkan sebagai air minum dan irigasi. Pada saat yang sama, air pasang dari laut akan terhambat dan tak mencemari sungai yang menjadi sumber utama air tawar masyarakat di sekitar sungai.\r\n\r\nSimpulan isi wacana di atas adalah...',
                'option_1' => 'Bendungan dari bantalan karet dapat membendung sungai',
                'option_2' => 'Bendungan dari bantalan karet sangat bermanfaat',
                'option_3' => 'Bendungan dari bantalan karet dapat mengalirkan air',
                'option_4' => 'Pemanfaatan air melalui bendungan bantalan karet',
                'correct_option' => 'Bendungan dari bantalan karet sangat bermanfaat',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Dalam proses komunikasi saya dengan seluruh rakyat Indonesia, baik melalui surat, telepon, maupun SMS, masih sering dirasakan adanya pelayanan yang kurang baik kepada rakyat. Oleh karena itu, mari kita tingkatkan kualitas pelayanan dengan penuh ketulusan, kasih sayang, dan rasa tanggung jawab.\r\n(Presiden Susilo Bambang Yudhoyono, pada acara HUT ke-59 Bhayangkara)\r\n\r\nPenggalan pidato tersebut bertujuan...',
                'option_1' => 'Mengecam kinerja POLRI yang kurang baik',
                'option_2' => 'Memberitahukan kepada rakyat agar menggunakan SMS, surat, maupun telepon dalam berkomunikasi dengan presiden',
                'option_3' => 'Mengimbau agar POLRI meningkatkan kualitas pelayanan kepada masyarakat dengan penuh tanggung jawab',
                'option_4' => 'Menenangkan hati rakyat dengan cara mengadakan komunikasi melalui surat, telepon, maupun SMS',
                'correct_option' => 'Mengimbau agar POLRI meningkatkan kualitas pelayanan kepada masyarakat dengan penuh tanggung jawab',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Produksi padi tahun 2005 diperkirakan mencapai 53,01 juta ton gabah kering giling atau turun dua persen dibandingkan dengan produksi tahun sebelumnya. Hal ini disebabkan adanya penurunan luas panen padi sawah akibat bencana banjir dan kekeringan, serta pergeseran waktu tanam pada empat bulan pertama di tahun 2005.\r\n\r\nGagasan utama paragraf di atas adalah...',
                'option_1' => 'Produksi padi tahun 2005 diperkirakan menurun',
                'option_2' => 'Produksi padi tahun 2005 diperkirakan mencapai 53,01 juta ton gabah kering atau turun dua persen dari sebelumnya',
                'option_3' => 'Adanya penyebab turunnya produksi padi, yaitu banjir dan kekeringan',
                'option_4' => 'Produksi padi tahun 2005 lebih jelek dibandingkan tahun sebelumnya',
                'correct_option' => 'Produksi padi tahun 2005 diperkirakan menurun',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Mereka selalu saja bermusuhan tidak pernah rukun. Sekarang mereka harus
                menanggung segala akibat dari perbuatannya.\r\n\r\nGurindam yang sesuai dengan ilustrasi di atas adalah...',
                'option_1' => 'Kurang pikir kurang siasat, tentu kelak dirimu tersesat',
                'option_2' => 'Siapa menggemari silang-sengketa, kelaknya pasti berdukacita',
                'option_3' => 'Membuat perkara amatlah mudah, jika terjadi timbullah gundah',
                'option_4' => 'Fikir dahulu sebelum berkata, supaya terelak silang-sengketa',
                'correct_option' => 'Siapa menggemari silang-sengketa, kelaknya pasti berdukacita',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Menurut hasil penelitian mahasiswa Institut Pertanian Bogor (IPB), daging ayam yang dijual pasaran di Jakarta, sebanyak 43% mengandung formalin, yaitu senyawa kimia yang mengandung zat karsiogenik. Zat ini ternyata dapat memicu penyakit kanker di dalam tubuh. Karena itu, para konsumen yang setiap hari belanja daging ayam, baik di pasar tradisional maupun modern agar waspada.\r\n\r\nKalimat tanya yang sesuai dengan isi paragraf di atas adalah...',
                'option_1' => 'Kapan para mahasiswa IPB mengadakan penelitian daging ayam yang mengandung zat karsiogenik?',
                'option_2' => 'Berapa orang mahasiswa yang melakukan penelitian terhadap daging ayam?',
                'option_3' => 'Apa tindak lanjut hasil penelitian mahasiswa IPB terhadap daging ayam?',
                'option_4' => 'Apa hasil penelitian mahasiswa IPB terhadap daging ayam yang dijual di pasar tersebut?',
                'correct_option' => 'Apa hasil penelitian mahasiswa IPB terhadap daging ayam yang dijual di pasar tersebut?',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Industri kimia dan petrokimia, industri pulp dan kertas, serta industri baja menggunakan minyak bumi sebgai bahan bakar utama untuk menggerakkan mesin-mesin pabrik. Demikian juga halnya alat transportasi laut, darat, dan udara, juga menggunakan minyak bumi sebagai bahan bakar utamanya.\r\n\r\nSimpulan umum paragraf di atas adalah...',
                'option_1' => 'Minyak bumi sebagai bahan bakar berbagai industri',
                'option_2' => 'Gas bumi dan batubara merupakan sumber energi yang penting',
                'option_3' => 'Alat transportasi menggunakan minyak bumi sebagai bahan bakar utama',
                'option_4' => 'Minyak bumi merupakan bahan bakar utama untuk berbagai macam industri dan alat transportasi',
                'correct_option' => 'Minyak bumi merupakan bahan bakar utama untuk berbagai macam industri dan alat transportasi',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Truk yang bermuatan cukup sarat itu gagal mendaki tanjakan licin pada penyeberangan sungai. Hujan lebat sebelumnya menyebabkan kondisi jalan sangat berat untuk dilewati, sehingga truk terperosok mundur ke tengah sungai. Agar memudahkan pendakian tanjakan, maka Ayub, pengemudi truk, meminta para penumpangnya turun. Dia bahkan mengingatkan kemungkinan terjadinya banjir bandang dari sebelah hulu. Akan tetapi, para penumpang menolak permintaan itu.\r\n\r\nIde pokok paragraf di atas adalah...',
                'option_1' => 'Truk gagal mendaki tanjakan licin',
                'option_2' => 'Truk terperosok mundur ke tengah sungai',
                'option_3' => 'Pengemudi truk meminta agar penumpang turun',
                'option_4' => 'Supir mengingatkan kemungkinan terjadinya banjir bandang',
                'correct_option' => 'Truk gagal mendaki tanjakan licin',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Jumlah angkatan kerja yang meningkat setiap tahun merupakan keuntungan sekaligus sebagai tantangan bagi pemerintah dan bangsa Indonesia dalam melaksanakan pembangunan nasional. Menurut pendapat para ahli bahwa manusia sebagai sumber potensial merupakan salah satu modal dasar pembangunan, yaitu sebagai motor penggerak dalam mekanisme kerja dalam proses produksi, serta sebagai sasaran dan hasil produksi itu sendiri.\r\n\r\nOpini (pendapat) yang terdapat dalam paragraf diatas adalah...',
                'option_1' => 'Jumlah angkatan kerja yang meningkat',
                'option_2' => 'Setiap tahun merupakan keinginan dan sekaligus sebagai tantangan',
                'option_3' => 'Melaksanakan pembangunan nasional',
                'option_4' => 'Manusia sebagai sumber potensi merupakan salah satu modal dasar pembangunan',
                'correct_option' => 'Manusia sebagai sumber potensi merupakan salah satu modal dasar pembangunan',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Faktor utama untuk bersaing adalah SDM yang sekaligus sebagai subjek dalam berproduksi. SDM perusahaan atau industri harus memiliki kemampuan teknis profesional dan adaptif. Kemampuan teknis profesional adalah keahlian menghasilkan barang dan jasa dengan sarana teknologi yang memadai. Kemampuan adaptif adalah kesanggupan SDM untuk menyesuaikan diri dengan lingkungan alam, sosial, dan lingkungan kerja, disiplin dan niai-nilai dalam perusahaan itu sendiri. Dengan kata lain, mereka harus memiliki kemampuan normatif.\r\n\r\nPenalaran yang terkandung dalam paragraf di atas adalah...',
                'option_1' => 'Deduktif',
                'option_2' => 'Induktif',
                'option_3' => 'Deduktif - induktif',
                'option_4' => 'Sebab - akibat',
                'correct_option' => 'Deduktif',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => Str::uuid(),
                'exam_id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'question' => 'Berbicara tentang pendidikan sebenarnya sama halnya dengan berbicara tentang kehidupan. Pendidikan merupakan proses yang dilakukan setiap individu menuju arah yang lebih baik sesuai dengan potensi kemanusiaan. Proses ini hanya berhenti ketika nyawa sudah tidak ada di dalam raga manusia. Pendidikan pada hakikatnya adalah proses memanusiakan manusia. Profesor Driyarkarya merumuskan pendidikan sebagai proses memanusiakan manusia muda, yakni suatu pengangkatan manusia muda ke taraf insani sehingga ia dapat menjalankan hidup sebagai manusia utuh dan membudayakan diri.\r\n\r\nIkhtisar kutipan paragraf tersebut adalah...',
                'option_1' => 'Pendidikan sangat diperlukan setiap individu dalam kehidupan bermasyarakat, terutama dalam menjadikan masyarakat berbudaya',
                'option_2' => 'Pendidikan adalah proses berkesinambungan dalam memanusiakan manusia menjadi manusia utuh dan berbudaya sesuai potensi yang dimiliki',
                'option_3' => 'Selama masih hidup, setiap individu memerlukan pendidikan dengan tujuan untuk memanusiakan dirinya agar menjadi manusia yang utuh dan menjadikan dirinya berbudaya',
                'option_4' => 'Pendidikan dan kehidupan ini tidak dapat dipisahkan karena pendidikan diperlukan oleh setiap individu untuk mencapai taraf insani, yakni sebagai manusia yang utuh dan membudayakan diri',
                'correct_option' => 'Pendidikan adalah proses berkesinambungan dalam memanusiakan manusia menjadi manusia utuh dan berbudaya sesuai potensi yang dimiliki',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
