<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('exams')->insert([
            [
                'id' => 'f16f2e1c-d001-49bc-ab89-8878a1624c65',
                'name' => 'Bahasa Indonesia',
                'date' => '2023-06-26',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => '7e34f1f7-7110-4ae7-94d1-c5b6eeedb0ce',
                'name' => 'Matematika',
                'date' => '2023-06-27',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
