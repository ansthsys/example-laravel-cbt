<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <main class="h-screen">
        <div class="flex flex-col sm:flex-row justify-center items-center h-full px-8 md:px-16 lg:px-40">
            <div class="basis-1/2 md:basis-5/12 w-10/12 flex justify-center items-center">
                <div
                    class="rounded-xl border-2 {{ Session::has('fail') ? 'border-red-400' : 'border-yellow-300' }} w-full md:w-10/12 px-5 py-16 md:py-10">
                    <form action="{{ route('register.post') }}" method="post">
                        @csrf
                        <div class="form-control w-full max-w-xs min-w-xs mx-auto">
                            <label class="label">
                                <span class="label-text text-lg font-medium">Name</span>
                            </label>
                            <input type="text" name="name" id="name" placeholder="Name"
                                class="input input-bordered w-full max-w-xs min-w-xs" required />
                        </div>
                        <div class="form-control w-full max-w-xs min-w-xs mx-auto">
                            <label class="label">
                                <span class="label-text text-lg font-medium">Email</span>
                            </label>
                            <input type="email" name="email" id="email" placeholder="Email"
                                class="input input-bordered w-full max-w-xs min-w-xs" required />
                        </div>
                        <div class="form-control w-full max-w-xs min-w-xs mx-auto">
                            <label class="label">
                                <span class="label-text text-lg font-medium">Password</span>
                            </label>
                            <input type="password" name="password" id="password" placeholder="Password"
                                class="input input-bordered w-full max-w-xs min-w-xs" required />
                        </div>
                        <div class="form-control w-full max-w-xs min-w-xs mx-auto mt-14 md:mt-10">
                            <input type="submit" class="btn btn-primary w-full max-w-xs min-w-xs" value="Register" />
                        </div>
                    </form>
                    <div class="form-control w-full max-w-xs min-w-xs mx-auto">
                        <p class="label-text mt-3">Login <a class="underline underline-offset-4"
                                href="{{ route('login') }}">here</a></p>
                    </div>
                </div>
            </div>
            <div class="hidden basis-1/2 md:basis-7/12 md:flex justify-center items-center">
                <div class="">
                    <img src="/images/college-entrance-exam-concept-illustration_114360-10502.webp" class=""
                        alt="Sample image" />
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="toast toast-end">
                @foreach ($errors->all() as $error)
                    <div class="alert alert-error">
                        <span>{{ $error }}</span>
                    </div>
                @endforeach
            </div>
        @endif
    </main>
</body>

</html>
