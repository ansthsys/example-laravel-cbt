<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @stack('scripts.exam')
</head>

<body class="antialiased">
    <div class="navbar bg-base-100 shadow-md md:px-32 sticky top-0">
        <div class="flex-1">
            <a type="button"
                class="normal-case text-xl font-bold {{ Route::currentRouteName() === 'exams.takeQuiz' ? 'btn-disabled' : '' }}"
                href="{{ route('dashboard') }}">
                {{ env('APP_NAME') }}
            </a>
        </div>
        <div class="flex-none">
            <div class="dropdown dropdown-end">
                <label tabindex="0" class="btn btn-ghost btn-circle avatar">
                    <div class="w-32 border-yellow-100 border-4 rounded-full">
                        <img src="https://th.bing.com/th/id/OIP.zFJrXaf1nz7rp6FGanMR_QHaHa?pid=ImgDet&rs=1" />
                    </div>
                </label>
                <ul tabindex="0"
                    class="mt-3 z-[1] p-2 shadow-lg menu menu-sm dropdown-content bg-base-100 rounded-box w-40">
                    <li class="my-1 text-center text-md font-semibold">{{ auth()->user()->name }}</li>
                </ul>
            </div>
            <div>
                <form action="{{ route('logout.post') }}" method="post">
                    @csrf
                    <input type="submit"
                        class="ms-5 mb-1 btn h-9 btn-sm btn-primary {{ Route::currentRouteName() === 'exams.takeQuiz' ? 'btn-disabled' : '' }}"
                        value="Logout">
                </form>
            </div>
        </div>
    </div>

    <main>
        @yield('main')
    </main>

    @stack('scripts')
</body>

</html>
