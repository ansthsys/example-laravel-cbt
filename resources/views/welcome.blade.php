@extends('layouts.app')

@section('main')
    <div class="px-5 md:px-40 py-20">
        <h2 class="text-4xl font-bold">Available Quiz</h2>
        <div
            class="mt-5 w-100 border-4 rounded-2xl px-3 md:px-8  py-10 flex flex-wrap justify-center lg:justify-start items-center gap-5">
            @foreach ($exams as $exam)
                <div
                    class="bg-gradient-to-r hover:underline-offset-4 from-orange-300 to-yellow-300 w-72 h-36 px-5 rounded-lg hover:scale-105 transition-all ease-in-out">
                    <a class="m-0 p-0" href="{{ route('exams.show', [$exam->id]) }}">
                        <div>
                            <p class="text-lg font-medium mt-16">{{ date('d M Y', strtotime($exam->date)) }}</p>
                            <h3 class=" text-2xl font-semibold">{{ $exam->name }}</h3>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
