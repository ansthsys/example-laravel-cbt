@extends('layouts.app')

@push('scripts.exam')
    <script type="text/javascript">
        window.onbeforeunload = function() {
            return 'Leave page during quiz'
        }
    </script>
@endpush

@section('main')
    <div class="w-11/12 md:w-7/12 mt-10 mb-16 mx-auto">
        <h1 class="text-4xl text-center mb-7">{{ $exam->name }}</h1>
        <form action="{{ route('exams.processQuiz', [$exam->id]) }}" method="POST">
            @csrf
            <input type="hidden" name="examId" value="{{ $exam->id }}">
            @foreach ($questions->shuffle() as $question)
                <div class="text text-justify px-5 py-8 mb-10 rounded-lg border-2">
                    <p>
                        {!! str_replace('\r\n', '<br>', $question->question) !!}
                    </p>
                    <div class="flex flex-col gap-1 py-2">
                        @foreach ($question->options as $option)
                            <div class="form-control m-0">
                                <label class="label justify-start gap-3 cursor-pointer">
                                    <input type="radio" name="{{ $question->id }}"
                                        class="radio radio-sm checked:bg-yellow-500" value="{{ $option }}" required />
                                    <span class="">{{ $option }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach

            <input style="float: right;" class="btn btn-primary mb-24" type="submit" value="Submit">
        </form>
    </div>
@endsection

@push('scripts')
@endpush
