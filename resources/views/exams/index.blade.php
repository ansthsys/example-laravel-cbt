@extends('layouts.app')

@push('scripts.exam')
    <script></script>
@endpush

@section('main')
    <div class="flex flex-col md:flex-row w-11/12 md:w-9/12 mx-auto">
        <div class="w-full md:w-1/2 py-20">
            <h2 class="text-4xl md:text-6xl font-bold">{{ $exam->name }}</h2>
            <h3 class="text-xl font-semibold">Total Question: {{ $total_question }}</h3>
            <form action="{{ route('exams.takeQuiz', [$exam->id]) }}" method="post">
                @csrf
                <button type="submit" class="mt-10 btn btn-primary">Take Quiz</button>
            </form>
        </div>
        <div class="w-full md:w-1/2 py-5">
            <div id="chart" class="mx-auto"></div>
            <div>
                <h4 class="text-md font-semibold text-center">History</h4>
                <div class="overflow-x-auto w-3/4 mx-auto">
                    <table class="table mt-2 rounded-xl">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        @foreach ($user_exam as $item)
                            <tr class="hover">
                                <td>{{ date('d M Y h:i', strtotime($item->date)) }}</td>
                                <td>{{ $item->value }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        const nameChart = {{ Js::from($exam->name) }};
        const labelChart = {{ Js::from($label_chart) }};
        const dataChart = {{ Js::from($data_chart) }};
        const containerChart = document.getElementById('chart');
        const chartOptions = {
            chart: {
                type: 'radar',
                toolbar: {
                    show: false
                }
            },
            series: [{
                name: nameChart,
                data: dataChart.length !== 0 ? dataChart : [0, 0, 0, 0, 0, 0],
            }],
            labels: labelChart.length !== 0 ? labelChart : ['A', 'B', 'C', 'D', 'E', 'F'],
            stroke: {
                show: true,
                width: 3,
                colors: ['yellow'],
                dashArray: 0
            },
            legend: {
                show: false
            }
        }
    </script>
    @vite('resources/js/apexchart.js')
@endpush
